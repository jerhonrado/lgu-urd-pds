  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      LGU Urdaneta
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="#">CSC</a>.</strong> All rights reserved.
  </footer>